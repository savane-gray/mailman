# usage:
#
# 1. make config PREFIX=dir [MAILMANSRC=dir] [NAME=VAL]...
#   Configure the system. PREFIX is installation prefix. MAILMANSRC is
#   the name of the Mailman source tree directory (must be writable).
#   Other meaningful variables are:
#
#     MAILMANDIR   - Top installation directory for mailman script.
#                    Default: $(PREFIX)/Mailman
#     BINDIR       - Installation directory for executable scripts.
#                    Default: $(PREFIX)/bin
#     HTDOCSDIR    - Installation directory for static HTML pages.
#                    Default: $(PREFIX)/htdocs
#     TEMPLATESDIR - Top installation directory for templates.
#                    Default: $(PREFIX)/templates
#     MANDIR       - Top installation directory for manual pages.
#                    Default: $(PREFIX)/man
#     CGIDIR       - Installation directory for listarchive CGI
#                    Default: $(PREFIX)/cgi-bin
#
#   If all these variables are supplied, PREFIX can be omitted.
#
# 2. make install
#   Installs everything.
#   Following targets can be used instead of install:
#
#     install-cgi       - Builds (if necessary) and installs the listarchive
#                         cgi.
#     install-Mailman   - Installs scripts in the Mailman subdirectory.
#     install-templates - Installs templates.
#     install-bin       - Installs scripts,
#     install-man       - Installs manpages.
#     install-htdocs    - Installs static HTML files.
#     
# 3. make listarchive
#   Builds the listarchive cgi in $(MAILMANSRC)/src.
#
# 4. make clean
#   Remove created files.
#
# 5. make distclean
#   Remove all created files and configuration cache.
#
# 6. make help
#   Shows this help list.
#
# usage ends

ifneq (,$(wildcard .config))
  include .config
else ifeq (,$(MAKECMDGOALS))
  # Nothing
else ifeq (,$(findstring $(MAKECMDGOALS),help config))
  $(error Please run make config first)
endif

help:
	@sed -n \
	     -e '/# usage ends/,$$d' \
             -e 's/^# //' \
             -e ta \
             -e 's/^#$$//' \
             -e ta \
             -e d \
             -e :a \
             -e '/^usage:/,$$p' $(firstword $(MAKEFILE_LIST))

DEFSRC = $(firstword $(wildcard /usr/src/mailman-[0-9][0-9.]*))

config:
	@(for var in PREFIX MAILMANDIR BINDIR HTDOCSDIR \
                   TEMPLATESDIR MANDIR CGIDIR;                       \
	do                                                           \
	    eval x=\$${$$var};                                       \
            if [ -n "$$x" ]; then                                    \
	        echo "$$var=$$x";                                    \
	    fi;                                                      \
	done;                                                        \
	if [ -n "$$MAILMANSRC" ]; then	                             \
	    echo "MAILMANSRC=$$MAILMANSRC";                          \
	elif [ -n "$(DEFSRC)" ]; then                                \
	    echo >&2 "assuming MAILMANSRC=$(DEFSRC)";                \
	    echo "MAILMANSRC=$(DEFSRC)";                             \
	else                                                         \
	    echo >&2 "Please set MAILMANSRC to point to the mailman-2.x tree"; \
            exit 1;                                                  \
	fi) > .config.tmp
	@if ! grep -q ^PREFIX .config.tmp && \
           test "$$(sed -r -n -e 's/^(MAILMANDIR|BINDIR|HTDOCSDIR|TEMPLATESDIR|MANDIR|CGIDIR)=.*/x/p' .config.tmp | tr -d '\n')" != xxxxxx; then    \
            echo >&2 "PREFIX must be supplied";                      \
	    rm .config.tmp;                                          \
            exit 1;                                                  \
	fi
	@mv .config.tmp .config

define build_cgi_rule = 
$(1):; @if [ -z "$(MAILMANSRC)" ]; then                                     \
	  echo >&2 "Please set MAILMANSRC to point to the mailman-2.x tree"; \
	  exit 1;                                                            \
	elif ! test -w $(MAILMANSRC)/src; then                               \
          echo >&2 "$(MAILMANSRC)/src must be writable";                     \
          exit 1;                                                            \
	else                                                                 \
          make -C $(MAILMANSRC)/src CGI_PROGS=$(1) $(1);                     \
        fi
endef

$(foreach tgt,listarchive findmail,$(eval $(call build_cgi_rule,$(tgt))))

define geninstall
install-$(1):;
	install -d $$(DESTDIR)$$($(shell echo $(1) | tr '[:lower:]' '[:upper:]')DIR)
	tar -C $(1) -cf - . | tar -C $$(DESTDIR)$$($(shell echo $(1) | tr '[:lower:]' '[:upper:]')DIR) -xpf -
endef

MAILMANDIR   = $(PREFIX)/Mailman
BINDIR       = $(PREFIX)/bin
HTDOCSDIR    = $(PREFIX)/htdocs
TEMPLATESDIR = $(PREFIX)/templates
MANDIR       = $(PREFIX)/man
CGIDIR       = $(PREFIX)/cgi-bin

TARGETS=\
  Mailman\
  templates\
  bin\
  man\
  htdocs

$(foreach tgt,$(TARGETS),$(eval $(call geninstall,$(tgt))))

install-cgi: listarchive findmail
	install -d $(DESTDIR)$(CGIDIR)
	install -o root -g mailman -m 2755 $(MAILMANSRC)/src/listarchive $(DESTDIR)$(CGIDIR)
	install -o root -g mailman -m 2755 $(MAILMANSRC)/src/findmail $(DESTDIR)$(CGIDIR)

install: install-cgi $(foreach tgt,$(TARGETS), install-$(tgt))

clean:
	@rm -f $(MAILMANSRC)/src/listarchive

distclean: clean
	@rm -f .config
