# GNU Mailutils -- a suite of utilities for electronic mail
# Copyright (C) 1999-2015 Free Software Foundation, Inc.
#
# GNU Mailutils is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU Mailutils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>.

"""Filter message through GNU Mailutils (sieve)."""

import os
import sys

from email.MIMEMessage import MIMEMessage
from email.MIMEText import MIMEText

from Mailman import mm_cfg
from Mailman import Errors
from Mailman import Utils
from Mailman import Message
from Mailman.Logging.Syslog import syslog
from Mailman.Handlers import Hold

from mailutils import stream, sieve
from mailutils.error import SieveMachineError,MU_ERR_PARSE

class SieveDiscard(Errors.DiscardMessage):
    """The message was discarded by Sieve"""
    reason = 'Sieve discarded this message'
    rejection = 'Your message has been discarded by mail filter'

class SieveHold(Errors.HoldMessage):
    """The message was held due to Sieve error"""
    reason = 'Your message was held for moderation because of' \
             ' internal software error or misconfiguration.' \
             ' Please, report this to the list owner.'

def process(mlist, msg, msgdata):
    if msgdata.get('approved'):
        return
    scriptbasename = mlist.internal_name() + '.siv'
    script = mm_cfg.SIEVE_SCRIPT_DIR + "/" + scriptbasename
    if not os.path.isfile(script):
        return
    syslog("post", "Sieve is using script file %s" % script)

    try:
        mu_msg = stream.MemoryStream(str(msg)).to_message()
        mu_sieve = sieve.Machine()
        mu_sieve.compile(script)
        mu_sieve.message(mu_msg)
    except SieveMachineError, e:
        lang = mlist.preferred_language
        nmsg = Message.UserNotification(mlist.GetOwnerEmail(),
                                        mlist.GetBouncesEmail(),
                                        'Sieve error notification',
                                        lang=lang)
        nmsg.set_type('multipart/mixed')
        if e.status == MU_ERR_PARSE:
            text = MIMEText(Utils.wrap('Sieve was unable to compile your script %s.  The reason given was as follows:\n\n%s\n\nFor convenience, the failed script is attached herewith.' % (scriptbasename, e.strerror)),
                         charset=Utils.GetCharSet(lang))
        else:
            text = 'Sieve failed to execute script %s:\n\n  %s.' % \
                   (script, e.strerror)
            text = MIMEText(Utils.wrap(text),
                             charset=Utils.GetCharSet(lang))

        nmsg.attach(text)
        nmsg.attach(MIMEText(open(script, 'r').read()))

        nmsg['X-Mailer'] = "Mailman Sieve Processor"
        nmsg.send(mlist)

        Hold.hold_for_approval(mlist, msg, msgdata, SieveHold)

    except Exception, e:
        Hold.hold_for_approval(mlist, msg, msgdata, SieveHold)

    if mu_msg.attribute.is_deleted():
        raise SieveDiscard
