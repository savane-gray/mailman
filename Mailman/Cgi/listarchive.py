# Copyright (C) 1998-2021 by the Free Software Foundation, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.

"""Provide a password-interface wrapper around private archives."""

import os
import errno
import sys
import cgi
import mimetypes
import time
import glob
import stat

from Mailman import mm_cfg
from Mailman import Utils
from Mailman import MailList
from Mailman import Errors
from Mailman import i18n
from Mailman.htmlformat import *
from Mailman.Logging.Syslog import syslog

# Set up i18n.  Until we know which list is being requested, we use the
# server's default.
_ = i18n._
i18n.set_language(mm_cfg.DEFAULT_SERVER_LANGUAGE)

SLASH = '/'



def true_path(path):
    "Ensure that the path is safe by removing .."
    # Workaround for path traverse vulnerability.  Unsuccessful attempts will
    # be logged in logs/error.
    parts = [x for x in path.split(SLASH) if x not in ('.', '..')]
    return SLASH.join(parts)[1:]



def guess_type(url, strict):
    if hasattr(mimetypes, 'common_types'):
        return mimetypes.guess_type(url, strict)
    return mimetypes.guess_type(url)


def collect_periods(privdir, listname):
    plist = []
    try:
        os.chdir(os.path.join(privdir, listname))
        matchlist = sorted(glob.glob("[0-9][0-9][0-9][0-9]-[0-9][0-9]"))
        for dir in matchlist:
            try:
                st = os.stat(dir)
                if stat.S_ISDIR(st.st_mode):
                    plist.append(dir)
            except os.error:
                continue
    except os.error as e:
        if errno.errorcode[e.errno] != errno.ENOENT:
            syslog('error', 'Error accessing "%s": %s\n',
                   os.path.join(privdir, listname), errno.errorcode[e.errno])

    return plist
    

script_name = 'listarchive'

def main():
    doc = Document()
    doc.set_language(mm_cfg.DEFAULT_SERVER_LANGUAGE)

    script_name = os.getenv('SCRIPT_NAME')
    script_base_name = script_name.split('/')[-1]
    
    privdir = os.path.join(mm_cfg.VAR_PREFIX, 'archives', 'html')
    form = cgi.FieldStorage()
    parts = Utils.GetPathPieces()

    listname = ''
    period = ''
    msgfile = ''
    if len(form):
        if form.has_key('l'):
            listname = form['l'].value
        if form.has_key('p'):
            period = form['p'].value
        if form.has_key('m'):
            msgfile = form['m'].value
        elif form.has_key('t'):
            if form['t'].value == 'date':
                msgfile = 'index.html'
            elif form['t'].value == 'thread':
                msgfile = 'threads.html'

        if listname:
            loc = '/' + listname
            if period:
                loc += '/' + period + '/'
                if msgfile:
                    loc += msgfile
            if not form.has_key('d'):
                print "Location: %s\n" % (Utils.ScriptURL('listarchive', absolute=True) + loc)
                return

    if parts:
        loc = ''
        nparts = len(parts);
        if nparts == 3:
            msgfile = parts[2]
        if nparts >= 2:
            period = parts[1]
        if nparts >= 1:
            listname = parts[0]

    if not listname:
        print 'Content-type: text/html\n\n'
        advertised = []
        listnames = Utils.list_names()
        for name in listnames:
            mlist = MailList.MailList(name, lock=0)
            if not mlist.advertised:
                continue
            try:
                file = os.path.join(privdir, name, '.timestamp')
                with open(file, 'r') as f:
		    r = int(f.readline().rstrip())
		    advertised.append((name,r))
            except IOError:
                continue
            
        advertised.sort(lambda x, y: int(y[1] - x[1]))
            
        listdir = ''
        even = 0
        for item in advertised:
            listdir += "<tr class=\"" + ("even" if even else "odd") + "\">\n" \
                    +  " <td><a href=\"%s/%s\">%s</a></td>\n" % (script_name, item[0], item[0]) \
                    +  " <td><a href=\"%s/%s/last/index.html\">Date</a></td>\n" % (script_name, item[0]) \
                    +  " <td><a href=\"%s/%s/last/threads.html\">Threads</a></td>\n" % (script_name, item[0]) \
                    +  " <td>%s</td>\n" % time.strftime('%c',time.localtime(item[1])) \
                    +  "</tr>\n";
            even = not even
            
        print Utils.maketext('list/listdir.html',
                            {'listdir': listdir})
        print Utils.maketext('list/footer.html',
                            {})
        return

    try:
        mlist = MailList.MailList(listname, lock=0)
    except Errors.MMListError, e:
        # Avoid cross-site scripting attacks
        safelistname = Utils.websafe(listname)
        msg = _('No such list <em>%(safelistname)s</em>')
        # FIXME: Private in the title
        doc.SetTitle(_("Private Archive Error - %(msg)s"))
        doc.AddItem(Header(2, msg))
        print doc.Format()
        syslog('error', 'No such list "%s": %s\n', listname, e)
        return

    if loc:
        plist = collect_periods(privdir, listname)
        try:
            n = plist.index(period)
        except ValueError:
            msg = _('No such period')
            doc.SetTitle(_("Archive Error - %(msg)s"))
            doc.AddItem(Header(2, msg))
            print doc.Format()
            syslog('error', 'No such period %s in list "%s"\n',
                   period, listname)
            return
        if form['d'].value == 'next':
            n = n + 1
        elif form['d'].value == 'prev':
            n = n - 1
        if n < 0 or n >= len(plist):
            print 'Content-type: text/html\n\n'
            print Utils.maketext('list/badperiod.html',
                                 {'listname'  : listname,
                                  'period' : period,
                                  'location' : loc,
                                  'script'  : script_name },
                                 mlist = mlist)
            print Utils.maketext('list/footer.html', {})
            return
        
        loc = '/' + listname + '/' + plist[n] + '?t=' + form['t'].value
        print "Location: %s\n" % (Utils.ScriptURL('listarchive', absolute=True) + loc)
        return
            
    
    realname = mlist.real_name
    
    i18n.set_language(mlist.preferred_language)
    doc.set_language(mlist.preferred_language)

    lang = 'en' # FIXME
    if mlist.archive_private:
        username = form.getvalue('username', '')
        password = form.getvalue('password', '')
        message = ''
        if not mlist.WebAuthenticate((mm_cfg.AuthUser,
                                      mm_cfg.AuthListModerator,
                                      mm_cfg.AuthListAdmin,
                                      mm_cfg.AuthSiteAdmin),
                                      password, username):
            if form.has_key('submit'):
                # This is a re-authorization attempt
                message = Bold(FontSize('+1', _('Authorization failed.'))).Format()
            # Output the password form
            charset = Utils.GetCharSet(mlist.preferred_language)
            print 'Content-type: text/html; charset=' + charset + '\n\n'
            # Put the original full path in the authorization form, but
            # avoid trailing slash if we're not adding parts.
            # We add it below.
            #action = mlist.GetScriptURL(script_base_name, absolute=1)
            action = script_name
            if parts:
                action = os.path.join(action, SLASH.join(parts[0:]))
            # Escape web input parameter to avoid cross-site scripting.
            print Utils.maketext('private.html',
                                 {'action'  : Utils.websafe(action),
                                  'realname': mlist.real_name,
                                  'message' : message,
                                 }, mlist=mlist)
            return
        lang = mlist.getMemberLanguage(username)
        
    i18n.set_language(lang)
    doc.set_language(lang)

    if not period:
        print 'Content-type: text/html\n\n'

        plist = collect_periods(privdir, listname)
        if len(plist) == 0:
            print Utils.maketext('list/empty.html',
                                 {'listname'  : listname })
        else:
            plist.sort()
            plist.reverse()

            options = ''
            for dir in plist:
                options += " <option value=\"%s\">%s</option>\n" % (dir, dir)
            print Utils.maketext('list/listindex.html',
                                 {'listname'  : listname,
                                  'options' : options,
                                  'listdir' : script_name,
                                  'script'  : script_name },
                                 mlist = mlist)
            print Utils.maketext('list/footer.html', {})
    else:
        if period == 'last':
            plist = collect_periods(privdir, listname)
            if len(plist) == 0:
                print 'Content-type: text/html\n\n'
                print Utils.maketext('emptyarchive.html',
                                     {'listname': realname,
                                      'listinfo': mlist.GetScriptURL('listinfo', absolute=1),
                                     }, mlist=mlist)
                return
            else:
                plist.sort()
                plist.reverse()
                period = plist[0]
        if not msgfile:
            msgfile = 'index.html'
        archive = os.path.join(privdir, listname, period, msgfile)
        try:
            f = open(archive, 'r')
        except IOError:
            msg = _('Private archive file not found')
            doc.SetTitle(msg)
            doc.AddItem(Header(2, msg))
            print doc.Format()
            syslog('error', 'Private archive file not found: %s', archive)
            return
        print 'Content-type: text/html\n\n'
        sys.stdout.write(f.read())
        f.close()
        
 
