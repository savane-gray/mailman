import os
import sys
import cgi
import mimetypes
import time
import glob
import stat

from Mailman import mm_cfg
from Mailman import Utils
from Mailman import MailList
from Mailman import Errors
from Mailman import i18n
from Mailman.htmlformat import *
from Mailman.Logging.Syslog import syslog

# Set up i18n.  Until we know which list is being requested, we use the
# server's default.
_ = i18n._
i18n.set_language(mm_cfg.DEFAULT_SERVER_LANGUAGE)

script_name = 'findmail'

def main():
    env = os.environ
    doc = Document()
    doc.set_language(mm_cfg.DEFAULT_SERVER_LANGUAGE)

    form = cgi.FieldStorage()
    listname = form.getvalue('idxname','')

    if listname:
        try:
            mlist = MailList.MailList(listname, lock=0)
        except Errors.MMListError, e:
            # Avoid cross-site scripting attacks
            safelistname = Utils.websafe(listname)
            msg = _('No such list <em>%(safelistname)s</em>')
            doc.SetTitle(_("Archive Error - %(msg)s"))
            doc.AddItem(Header(2, msg))
            print doc.Format()
            syslog('error', 'No such list "%s": %s\n', listname, e)
            return

        if mlist.archive_private:
            username = form.getvalue('username', '')
            password = form.getvalue('password', '')
            message = ''
            if not mlist.WebAuthenticate((mm_cfg.AuthUser,
                                          mm_cfg.AuthListModerator,
                                          mm_cfg.AuthListAdmin,
                                          mm_cfg.AuthSiteAdmin),
                                          password, username):
                if form.has_key('submit'):
                    # This is a re-authorization attempt
                    message = Bold(FontSize('+1', _('Authorization failed.'))).Format()
                # Output the password form
                charset = Utils.GetCharSet(mlist.preferred_language)
                print 'Content-type: text/html; charset=' + charset + '\n\n'
                # Put the original full path in the authorization form, but
                # avoid trailing slash if we're not adding parts.
                # We add it below.
                action = mlist.GetScriptURL(script_name, absolute=1) + \
                           '?' + os.getenv('QUERY_STRING')
                # Escape web input parameter to avoid cross-site scripting.
                print Utils.maketext('private.html',
                                     {'action'  : Utils.websafe(action),
                                      'realname': mlist.real_name,
                                      'message' : message,
                                     }, mlist=mlist)
                return

    finder = os.getenv('MAIL_FINDER_CGI')
    os.execl(finder, os.path.basename(finder))
