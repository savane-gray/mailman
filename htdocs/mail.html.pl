<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>mail.gnu.org.ua</title>
    
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />

  <link rel="stylesheet" type="text/css" href="/mail.css" />
  <link rev="made" href="mailto:gray@gnu.org.ua" />
</head>


<body>

<h1>Witaj w <span class="host">mail.gnu.org.ua</span></h1>  
  
<ul class="nav">
  <li class="navtopitem"><span class="navtoptitle">Listy wysyłkowe</span>
    <ul class="navsubmenu" id="submenu1">
      <li class="navsubitem">
	<a href="/mailman/listinfo/">Katalog</a>
      </li>
      <li class="navsubitem">
	<a href="/mailman/admin/">Zarządzanie</a>
      </li>
      <li class="navsubitem">
	<a href="/mailman/listarchive/">Archiwa</a>
      </li>
    </ul>
  </li>

  <li class="navtopitem"><span class="navtoptitle">GNU</span>
    <ul class="navsubmenu">
      <li class="navsubitem">
	<a href="http://puszcza.gnu.org.ua">Puszcza</a>
      </li>
      <li class="navsubitem">
        <a href="http://www.gnu.org.ua">GNU na Ukrainie</a>
      </li>
      <li class="navsubitem">
        <a href="http://ftp.gnu.org.ua">GNU FTP</a>
      </li>
    </ul>
  </li>
	
  <li class="navtopitem"><span class="navtoptitle">Inne odsyłacze</span>
    <ul class="navsubmenu">
      <li class="navsubitem">
	<a href="http://gray.gnu.org.ua">Administrator</a>
      </li>
      <li class="navsubitem">
        <a href="https://mail.gnu.org.ua/user/">Poczta użytkownika</a>
      </li>
      <li class="navsubitem">
        <a href="http://validator.w3.org/check/referer">Sprawdz XHTML 1.0</a>
      </li>
    </ul>
  </li>

  <li class="navtopitem translations"><span class="navtoptitle">Tłumaczenia</span>
    <ul class="navsubmenu">
      <li class="navsubitem">
	<a href="en/mail.html">English</a>
      </li>
      <li class="navsubitem">
        <a href="uk/mail.html">Українська</a>
      </li>
    </ul>
  </li>
</ul>    

<div id="main">
  <h2>Co to jest <span class="host">mail.gnu.org.ua?</span></h2>

  <p>
    Serwer <span class="host">mail.gnu.org.ua</span> jest centrum
    obsługi list, których używa system 
    <a href="http://puszcza.gnu.org.ua">Puszcza.gnu.org.ua</a>
    oraz różne projekty tam zamieszczone. Listy wysyłkowe są
    wspaniałym sposobem uzyskania pomocy technicznej, zgłoszenia
    błędów oprogramowania oraz zamieszczenia swoich komentarzy i
    propozycji.
  </p>

  <h2>Subskrybowanie i zarządzanie listami</h2>

  <p>
    Aby dostawać wiadomości z listy należy ją zasubskrybować.
    Niektóre listy są skonfigurowane w sposób, który umożliwia
    dostarczanie do nich tylko wiadomości wysłanych
    prenumeratorami. W takim wypadku reszta wiadomości może być
    skierowana do kolej moderacji. Dokładne informacje o każdej
    liście, włącznie z URL-em subskrybowania, można otrzymać w
    <a href="/mailman/listinfo/">katalogu
    list</a>.
  </p>

  <p>
    Subskrybenci list znajdą na stronach katalogu możliwość zmiany
    swoich ustawień. Na przykład, mogą skonfigurować swoją
    subskrypcję tak, aby dostawać całą dzienną pocztę z listy w jednej
    wiadomości, zamiast zwykłego dostarczania pojedynczych  wiadomości.
  </p>

  <p>
    Jeśli jesteś administratorem projektu i chcesz utwórzyć
    listę wysyłkową, odwiedź stronę <a href="http://puszcza.gnu.org.ua">
    interfejsu zarządzania</a> Twoim projektem, wybierz "<i>Listy
    wysyłkowe</i>" w pasku "<i>Zarządzanie</i>" a następnie kieruj
    się instrukcjami, które znajdziesz na tamtej stronie. W wypadku
    gdyby żadna z zaproponowanych przez nią nazw Ci nie pasowała,
    napisz do <a href="mailto:savannah-hackers@gnu.org.ua">
    administratorów systemu</a> i poproś ich o utwórzenie listy.
  </p>

  <p>
    Administratrzy projektów mogą również
    <a href="/mailman/admin/">zarządzać</a> swoimi
    listami wysyłkowymi.
  </p>

  <h2>Archiwa</h2>

  <p>
    Nasza witryna umożliwia również dostęp do
    <a href="/pipermail/">archiwów</a> list
    wysyłkowych. Archiwa dostarczają możliwość szukania według
    różnych kriteriów, np. daty, wątku, itp.
  </p>
</div> <!-- main -->  

<div id="footer">
<address>Copyright &copy; 2006, 2010 Sergey Poznyakoff</address>.
<p>
Zezwala się na wykonywanie i dystrybucję wiernych kopii tego
tekstu, bez tantiem, niezależnie od nośnika, pod warunkiem
zachowania niniejszego zezwolenia oraz informacji o prawach
autorskich.
</p>
<p>
  Pytania w sprawie systemu pocztowego <span class="host">gnu.org.ua</span>
oraz systemu list wysyłkowych prosimy wysyłać do
  <a href="mailto:postmaster@gnu.org.ua"><em>postmastera</em></a>.
</p>  
<p>
Informacje o niedziałających odnośnikach oraz propozycje dotyczące tej
strony prosimy wysyłać
do <a href="mailto:webmasters@gnu.org.ua"><em>webmasterów</em></a>.
</p>
</div>

</body>
</html>
